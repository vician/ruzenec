## Živý růženec

Živý růženec je modlitební společenství věřících, kteří se kromě svých obvyklých modliteb zavázali k denní modlitbě jednoho desátku růžence. Tato stránká může pomoci při organizaci takových to skupinek.

## Návod na použití

Na stránce [skupina](skupina) si můžete vytvořit odkaz pro svoji skupinu a ten si rozeslat mezi členy.


## Kontakt

[info@zivyruzenec.cz](mailto:info@zivyruzenec.cz)
