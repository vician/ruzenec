---
title: O živém růženci
---

## Co to je?
Živý růženec je modlitební společenství věřících, kteří se kromě svých obvyklých modliteb zavázali k denní modlitbě jednoho desátku růžence. Desátky jsou ve společenství rozděleny mezi členy tak, aby se skupina v souhrnu pomodlila za den celý růženec.

## Jaký to má smysl?
Smyslem je především síla společné modlitby. Dá-li se nás dohromady 5, pomodlíme se společně každý den jeden růženec. Jeden desátek se snadno každý pomodlí a především v některých důležitých nebo bolestných událostech života je pěkné a posilující, když se všichni shodnou na jednom úmyslu a prosí za něj Boha. Přínosem pro každého, kdo se zapojí, je upevnění své vytrvalosti v modlitbě.

## Na jaký úmysl se budeme modlit?
Boží náruč je dost široká na to, aby se do ní vešlo všechno, co do ní prostřednictvím modlitby vložíme. Kromě svých vlastních osobních proto zařazujeme také úmysl společný pro celou růžencovou skupinku.

V každé růžencové skupince (= 5 lidí) se každý týden prohodí desátky. Vždy ten, kdo se modlí první desátek, zadá úmysl společné modlitby. To znamená, že do aplikace na tomto webu napíše úmysl na další týden, který se zobrazí všem členům skupinky. Je vynikající, když můžu někoho požádat, aby se modlil za to, co mi zrovna leží na srdci - a prostřednictvím Živého růžence můžeme vzájemně sdílet své modlitební úmysly.

## Zaujalo Tě to?
Můžeš se ještě podívat na náhled aplikace a chceš-li, můžeš se stát členem. Budeš vítán.
Pokud máš nějakou otázku, neboj se nás kontaktovat.

## Co řekli o růženci a o modlitbě?
- Růženec je má nejmilejší modlitba. Je to modlitba podivuhodná. Podivuhodná svou prostotou a hloubkou. Na pozadí Zdrávasů se před námi definují hlavní události ze života Ježíše Krista. (Jan Pavel II.)
- Proste a bude vám dáno! (Mt 7, 7) Dostane se vám všeho, zač budete prosit. (Mt 21, 22)
- Svatý růženec je jedinečnou kontemplativní modlitbou, při níž, vedeni nebeskou Matkou Pána, upíráme oči na Tvář Vykupitele, abychom byli připodobněni jeho tajemství radosti, světla, bolesti a slávy. (Benedikt XVI.)
- Dívám se na vás na všechny, bratři a sestry každého stavu ... a také na mládež: vezměte růženec s důvěrou opět do rukou a objevte tuto modlitbu znovu ve světle Písma, v souladu s liturgií a v kontextu každodenního života. (Jan Pavel II.)

## Jak se desátek živého růžence modlí

- Desátek růžence
	- Modlitba Páně
	- 10× Zdrávas Maria
	- Sláva Otci ...
	- Pane Ježíši ...
- Tajemství růžence Radostného
	- kterého jsi z Ducha svatého počala
	- s kterým jsi Alžbětu navštívila
	- kterého jsi v Betlémě porodila
	- kterého jsi v chrámě obětovala
	- kterého jsi v chrámě nalezla
- Tajemství růžence Světla
	- který byl pokřtěn v Jordánu
	- který zjevil v Káně svou božskou moc
	- který hlásal Boží království a vyzýval k pokání
	- který na hoře Proměnění zjevil svou slávu
	- který ustanovil Eucharistii
- Tajemství růžence Bolestného
	- který se pro nás krví potil
	- který byl pro nás bičován
	- který byl pro nás trním korunován
	- který pro nás nesl těžký kříž
	- který byl pro nás ukřižován
- Tajemství růžence Slavného
	- který z mrtvých vstal
	- který na nebe vstoupil
	- který Ducha svatého seslal
	- který tě, Panno, do nebe vzal
	- který tě v nebi korunoval
