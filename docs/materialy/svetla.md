---
title: Světla
---
_Zdroj: [Salesiáni spolupracovníci](http://www.ascczech.cz/) - [Formační plány na roky 2010 a 2011](http://www.ascczech.cz/rs/download.php?sekce=5)._

## I. který byl pokřtěn v Jordánu
1. jehož evangelizaci předcházel skrytý život
2. který vyrůstal v rodině Marie a Josefa
3. který byl stále v modlitbě ve spojení se svým Otcem
4. který pokorně snášel lidské nedokonalosti
5. který i ve všedních úkonech projevoval poslušnost Otci
6. který trpělivě čekal až přijde jeho čas
7. který byl Duchem svatým uveden do apoštolátu
8. který se plně odevzdal do vůle Otce
9. který hlásal radostnou zvěst
10. který se vrátil od Jordánu plný Ducha sv.

## II. který zjevil v Káně svou božskou moc
1. který byl pozván s Marií a učedníky na svatbu
2. na kterém jsi poznala, že nadešla Otcem stanovená chvíle zázraku
3. jemuž jsi připomněla, že chybí víno
4. který nechal naplnit džbány k očišťování vodou
5. který učinil první zázrak na Tvou přímluvu
6. jehož zázraky zůstaly před mnohými skryty
7. který žádá naši připravenost k plnění jeho vůle
8. který k nám mluví, aby prorazil naši hluchotu
9. který nám vnuká, co a jak máme činit
10. který proměňuje náš život v novou kvalitu

## III. který hlásal Boží království a vyzýval k pokání
1. který byl vyvoleným národem dlouho očekáván
2. který přichází vysvobodit lidstvo z otroctví hříchu
3. který se stává nadějí na lepší život
4. který přináší radostnou zvěst, jíž je evangelium
5. který nás osvobozuje z područí druhých i ze zajetí zlých náklonností
6. který přišel hlásat nejen evangelium, ale také i pokání
7. jehož přítomnost je spojena s naší proměnou srdce
8. který se zcela vydává, ale také současně vyžaduje naši otevřenost
9. jehož láska stravuje, ale také rozněcuje a činí šťastným
10. s jehož radostným evangeliem kéž přijmeme i svůj každodenní kříž

## IV. který na hoře Proměnění zjevil svou slávu
1. který se ukazuje svým apoštolům ve slávě a moci dříve, než ho uvidí slabého a zmučeného
2. který dává poznat i se svým Otcem svoji Božskou svrchovanost
3. který vštěpuje do srdcí apoštolů, že je nesmrtelný a že se nemusí obávat jeho zdánlivé prohry
4. který odhalil apoštolům tajemství své velikosti
5. který zjevil apoštolům plnost svého oslaveného zduchovnělého těla
6. který před svým utrpením posiluje víru apoštolů
7. který si přál, aby událost proměnění zůstala do stanovené doby tajemstvím
8. který i nás zve na naši horu Tábor, abychom s ním zažili vnitřní, osobní a intimní setkání
9. jehož velikost naplnila apoštoly bázní
10. v jehož blízkosti zakoušíme mocnou ochranu

## V. který ustanovil Eucharistii
1. který ač mocný, věčný a vznešený se zcela vydal do rukou hříšným lidem
2. který raději trpí nebezpečím zneužití, než aby se od nás natrvalo vzdálil
3. který přichází v eucharistii jako bezmocné dítě, abychom se ho nebáli
4. který přichází v eucharistii v naprosté tichosti
5. který si nenechává sloužit, ale sám slouží
6. který se sám stává našim pokrmem
7. který sytí naši duši a naplňuje všechny její potřeby
8. který svatým přijímáním udržuje náš život a převádí ho do věčnosti
9. jehož obdarování převyšuje naše představy
10. který v eucharistii uzdravuje naše duše i těla

