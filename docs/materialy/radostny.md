---
title: Radostný
---
_Zdroj: [Salesiáni spolupracovníci](http://www.ascczech.cz/) - [Formační plány na roky 2010 a 2011](http://www.ascczech.cz/rs/download.php?sekce=5)._

## I. Kterého jsi, Panno, z Ducha svatého počala
1. jehož příchod přislíbil Bůh v ráji. (Gn 3,15)
2. kterého předpověděli proroci. (Iz 7,14)
3. kterému Bůh slíbil trůn jeho předka Davida. (L 1,32)
4. který spasí svůj lid od všeho hříchu. (Mt 1,21)
5. jehož početí ti oznámil anděl. (L 1,28)
6. o němž anděl řekl: „Bude veliký a nazýván Syn Nejvyššího“. (L 1,32)
7. při jehož početí na tebe sestoupil Duch Svatý. (L 1,35)
8. při jehož početí tě zastínila moc Nejvyššího. (L 1,35)
9. při jehož početí jsi řekla: „Jsem služebnice Pána“. (L 1,38)
10. při jehož početí jsi řekla: „Ať se stane podle tvého slova“. (L 1,38)

## II. Se kterým jsi Alžbětu navštívila
1. s kterým ses vydala po andělově zvěstování na cestu. (L 1,39)
2. s kterým jsi spěchala do jednoho judského města v horách. (L 1,39)
3. s kterým jsi pozdravila Alžbětu. (L 1,10)
4. s kterým ses modlila: „Velebí má duše Pána“. (L 1,46) 
5. s kterým ses modlila: „Od této chvíle mě budou blahoslavit všechna pokolení“. (L 1,48)
6. s kterým ses modlila: „Veliké věci mi učinil ten, který je mocný“. (L 1,49)
7. s kterým ses modlila: „Mocné svrhl s trůnu a ponížené povýšil“. (L 1,52)
8. s kterým ses modlila: „Hladové nasytil dobrými věcmi a bohaté propustil s prázdnou“. (L 1,53)
9. s kterým jsi u Alžběty zůstala tři měsíce. (L 1,56)
10. kterému připravoval cestu Alžbětin syn Jan. (L 1,76)

## III. Kterého jsi v Betlémě porodila
1. s kterým jsi šla do Betléma, abyste se s Josefem dali zapsat. (L 2,5)
2. pro kterého neměli místa. (L 2,7)
3. kterého jsi porodila v Betlémě judském. (Mt 2,1 )
4. kterého jsi zavinula do plenek a položila do jeslí. (L 2,7 )
5. který byl tvým prvorozeným synem. (L 2,7)
6. při jehož narození andělé chválili Boha. (L 2,13 )
7. kterému se přišli poklonit pastýři. (L 2,16)
8. kterému mudrci z východu přinesli dary. (Mt 2,11)
9. po kterém Herodes pátral, aby ho zahubil. (Mt 2,13)
10.	kterého jsi s Josefem zachránila útěkem do Egypta. (Mt 2,14) 

## IV. Kterého jsi v chrámě obětovala
1.	který byl osmého dne obřezán. (L 2,21)
2.	kterého jsi přinesla do Jeruzaléma, abys ho představila Hospodinu. (L 2,22) 
3.	za kterého byla dána oběť, jak je nařízeno v Zákoně. (L 2,24)
4.	kterého vzal Simeon do náručí. (L 2,28)
5.	který je spása připravená přede všemi národy. (L 2,30)
6.	který je světlo k osvícení pohanů. (L 2,32)
7.	který je znamení, jemuž se bude odpírat. (L 2,34)
8.	který je určen k pádu a pozvednutí mnohých. (L 2,34)
9.	o kterém mluvila prorokyně Anna ke všem, kdo očekávali vykoupení Jeruzaléma. (L 2,38)
10. s kterým jste se vrátili do Nazareta. (L 2,39) 

## V. Kterého jsi v Chrámě nalezla
1.	který rostl a sílil a byl plný moudrosti. (L 2,40)
2.	kterého jsi vzala s sebou do Jeruzaléma, když mu bylo dvanáct let. (L 2,42)
3.	který zůstal v Jeruzalémě a rodiče to nezpozorovali. (L 2,43)
4.	kterého jsi s Josefem hledala mezi příbuznými a známými. (L 2,44)
5.	kterého jsi po třech dnech našla v chrámě. (L 2,46)
6.	který seděl mezi učiteli, poslouchal je a dával jim otázky. (L 2,47)
7.	nad jehož odpověďmi a chápavostí všichni žasli. (L 2,47)
8.	kterému jsi řekla: „Synu, proč jsi nám to udělal?“ (L 2,48)
9.	který řekl: „Proč jste mne hledali? Což jste nevěděli, že já musím být tam, kde jde o věc mého Otce?“ (L 2,49)
10.	který se se svými rodiči vrátil do Nazareta a poslouchal je. (L 2,51)
