---
title: Slavný
---
_Zdroj: [Salesiáni spolupracovníci](http://www.ascczech.cz/) - [Formační plány na roky 2010 a 2011](http://www.ascczech.cz/rs/download.php?sekce=5)._

## I. který z mrtvých vstal
1.	který třetího dne vstal z mrtvých ve shodě s Písmem. (1K 15,4)
2.	který se po svém zmrtvýchvstání zjevil ženám. (Mt 28,9)
3.	který se zjevil svým učedníkům. (Mt 28,17)
4.	který se zjevil i nevěřícímu Tomášovi. (J 20,26)
5.	který řekl: „Blahoslavení, kteří neviděli a přesto uvěřili“. (J 20,29)
6.	nad kterým už smrt nemá vládu. (Ř 6,9)
7.	který řekl: „Já jsem vzkříšení a život“. (J 11,25)
8.	který řekl: „Kdo věří ve mne, i kdyby zemřel, bude živ“. (J 11,25)
9.	který z mrtvých vstal, jakožto první z těch, kteří zesnuli. (1K 15,20)
10.	s kterým se i my jednou ukážeme ve slávě. (Ko 3,4)

## II. který na nebe vstoupil
1.	který se čtyřicet dní ukazoval učedníkům a vykládal jim o Božím království. (Sk 1,3)
2.	který se odloučil od apoštolů, zatímco jim žehnal. (L 24,51)
3.	kterého vzal oblak apoštolům z očí. (Sk 1,9)
4.	který byl vzat do nebe. (Sk 1,11)
5.	který je po Boží pravici a ujímá se nás. (Ř 8,34)
6.	který přijde právě tak, jako ho apoštolové viděli odcházet. (Sk 1,11) 
7.	který řekl: „Odcházím, abych vám připravil místo.“ (J 14,2)
8.	který řekl: „ V domě mého Otce je mnoho příbytků.“ (J 14,2)
9.	který řekl: „Já jsem s vámi po všechny dny až do konce světa.“ (Mt 28,20)
10. který je stejný včera, dnes i na věky. (Žd 13,8)

## III. který Ducha svatého seslal
1.	který byl pomazán Duchem svatým a mocí. (Sk 10,38)
2.	který slíbil apoštolům Zastánce, Ducha pravdy. (J 15,26)
3.	který řekl: „Až přijde Duch pravdy, uvede vás do celé pravdy.“ (J 16,13)
4.	jehož Duch naplnil apoštoly, takže promlouvali z vnuknutí. (Sk 2,4)
5.	který v hojnosti vylil Ducha svatého. (Sk 2,33)
6.	jehož Duch vedl apoštoly. (Sk 16,7)
7.	jehož apoštolové udíleli Ducha svatého vkládáním rukou. (Sk 8,17)
8.	který nám dal Ducha jako záruku. (2K 5,5)
9.	jehož duch volá v našem srdci „Abba, Otče!“ (Ga 4,6)
10.	v jehož jménu byl Duch svatý seslán. (J 14,26)

## IV. který tě, Panno, do nebe vzal
1.	který tě od věčnosti vyvolil za svou matku. (srov. Gn 3,15)
2.	který tě uchránil od dědičného hříchu. (Pius IX)
3.	který se z tebe narodil. (Ga 4,4)
4.	který vyplnil tvou prosbu na svatbě v Káně. (J 2,3-5)
5.	kvůli němuž tě blahoslavili. (L 11,27)
6.	který ti svěřil svého nejmilejšího učedníka. (J 19,26)
7.	který tě dal za matku nám všem. (J 19,27)
8.	který nedopustil, aby tvé tělo podlehlo porušení. (Pius XII)
9.	který tě vzal s tělem i duší do nebeské slávy. (Pius XII)
10.	s kterým záříš v nebi krásou těla i duše. (Pius XII)

## V. který tě v nebi korunoval
1.	který tě v nebi korunoval.
2.	s kterým v nebi vládneš.
3.	u kterého se za nás přimlouváš.
4.	který nás svěřil do tvé mateřské péče.
5.	který v tobě dal církvi příklad víry a vytrvalé modlitby.
6.	který tě zahrnul slávou.
7.	který přijde na konci časů.
8.	který bude soudit živé i mrtvé.
9.	v němž bude všechno obnoveno.
10.	na jehož příchod čekáme.
