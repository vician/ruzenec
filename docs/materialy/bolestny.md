---
title: Bolestný
---
_Zdroj: [Salesiáni spolupracovníci](http://www.ascczech.cz/) - [Formační plány na roky 2010 a 2011](http://www.ascczech.cz/rs/download.php?sekce=5)._

## I. který se pro nás krví potil
1.	který se po večeři odebral s učedníky na Olivovou horu (Lk 22,39)
2.	který se šel modlit a vzal Petra, Jakuba a Jana s sebou (Mt 26,36)
3.	který trochu poodešel a padl tváří k zemi (Mt 26,39)
4.	který se modlil:„Otče můj, jestliže je to možné, ať mne mine tento kalich, ale ne jak já, ale jak ty chceš“ (Mt 26,39)
5.	jehož pot stékal na zem jako krůpěje krve (Lk 22,44)
6.	který se vrátil k učedníkům a shledal, že spí (Mt 26,40)
7.	který řekl těm třem:„to jste nemohli ani jednu hodinu se mnou bdít“? (Mt 26,40) 
8.	který se podruhé modlil: „Otče, ať se stane tvá vůle“ (Mt 26,42)
9.	který shledal, že učedníci zase spí (Mt 26,43)
10.	který nechal učedníky a po třetí se modlil stejnými slovy (Mt 26,44)

## II. který byl pro nás bičován
1. kterého dali v poutech odvést k Pilátovi (Mt 15,1)
2. kterého se Pilát otázal: „Ty jsi židovský král?“ (Mk 15,2)
3. který mu odpověděl „Ano, já jsem král… Moje království není z tohoto světa.“ (Jan 18,36–37)
4. o kterém Pilát řekl židům: „Já na něm neshledávám žádnou vinu.“ (Jan 18,38)
5. kterého poté Pilát vzal a dal ho zbičovat (Jan 19,1)
6. který neměl podobu ani krásu, opovržený, opuštěný od lidí, muž bolesti. 
(Iz 53,2–3)
7. který nesl naše utrpení, obtížil se našimi bolestmi. (Iz 53,4)
8. který byl rozdrcen pro naše hříchy, jeho rány nás uzdravily. (Iz. 53,5)
9. který byl týrán, ale podrobil se a neotevřel svá ústa. (Iz 53,7)
10. který byl jako beránek vedený na porážku, neotevřel svá ústa. (Iz 53,7)

## III. který byl pro nás trním korunován
1. kterého vojáci svlékli a přehodili mu nachově rudý plášť.(Mt 27,28)
2. kterému upletli korunu z trní a vsadili mu ji na hlavu.(Mt27,29)
3. kterému do pravé ruky dali rákosovou hůl.(Mt 27,29)
4. kterému se posmívali, plivali na něj a bili ho po hlavě.(Mt 27,29–30)
5. o kterém Pilát řekl: „Nemám vinu na krvi tohoto člověka. To je vaše věc“. 
(Mt 27,24)
6. který vyšel s trnovou korunou a v rudém plášti. (Jan 19,5)
7. o kterém Pilát řekl: „Hle člověk!“ (Jan 19,5)
8. o kterém lid začal křičet: „Ukřižuj ho, ukřižuj!“ (Jan 19,6)
9. místo kterého chtěli propustit Barabáše. (Lk 23,18)
10. o kterém lid řekl: „Jeho krev ať padne na nás a na naše děti! (Mt 27,25)

## IV. který pro nás nesl těžký kříž
1.	Pána Ježíše odsoudili na smrt. – „Nesuďte, a nebudete souzeni.“ (Lk 6,37)
2.	Pán Ježíš přijímá kříž. – „Vezměte na sebe mé jho a učte se ode mě, neboť jsem tichý a pokorný srdcem.“ (Mt 11,29)
3.	Pán Ježíš padá poprvé pod křížem. – „Zapři sám sebe, vezmi svůj kříž a následuj mě.“ (Mk 8,34)
4.	Pán Ježíš potkává svoji matku. – „Tvou vlastní duší pronikne meč..“ (Lk 2,35)
5.	Šimon pomáhá Pánu Ježíši nést kříž. – „Jeden druhého břemena neste, a tak maplníte Kristův zákon.“ (Gal 6,2)
6.	Veronika podává Pánu Ježíši roušku. – „Cokoliv jste udělali pro jednoho z mých nejposlednějších bratří, pro mne jste udělali.“ (Mt 25,40)
7.	Pán Ježíš padá po druhé. – „Kdo stojí, ať si dá pozor, aby nepadl.“ 
(1Kor 10,12)
8.	Ženy pláčou nad Ježíšem. – „Jeruzalémské dcery, neplačte nade mnou! 
Spíše nad sebou plačte a nad svými dětmi!“ (Lk 23,28)
9.	Ježíš padá potřetí pod křížem. – „Děje-li se toto se stromem zeleným, co se stane se suchým!“ (Lk 23,31)
10.	Pána Ježíše zbavili roucha. – „Nikdo nemá větší lásku než ten, kdo za své přátele položí svůj život.“ (Jan 15,13)

## V. který byl pro nás ukřižován
1. kterého dovedli na místo, které se nazývá Lebka, ukřižovali jeho a dva zločince. (Lk 23,23)
2.	který řekl: „Otče, odpusť jim, vždyť nevědí, co činí.“ (Lk 23,24)
3.	„Žízním!“ (Jan 19,28)
4.	ke kterému se obrátil lotr se slovy „Ježíši, pamatuj na mě, až přijdeš do svého království.“ Odpověděl mu: „Amen, pravím ti: Dnes budeš se mnou v ráji.“ 
(Lk 23,42–43)
5.	který řekl matce: „Ženo, to je tvůj syn.“ A Janovi: „To je tvá matka.“ 
(Jan 19,26–27)
6.	který zvolal „Bože můj, Bože můj, proč jsi mě opustil?“ (Mt 27,46)
7.	„Dokonáno je!“ (Jan 19,30)
8.	„Otče, do tvých rukou poroučím svého ducha!“ (Lk 23,46)
9.	jehož mrtvé tělo položili matce na klín.
10.	který položil za nás svůj život. (1 Jan 3,16)
