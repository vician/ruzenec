---
title: Kněžská povolání
---
_Zdroj: [Arcibiskupský seminář Praha](http://http//www.arcs.cuni.cz/) - [Ukázka rozjímavého růžence za kněžská povolání](http://www.arcs.cuni.cz/index.php?id=5&id2=16)_

## I . desátek
1. Ježíš, který řekl Natanaelovi: „Viděl jsem tě dříve, než tě Filip zavolal, když jsi byl pod fíkovníkem.“ (J 1, 48)
2. Ježíš, který řekl Filipovi: „Pojď za mnou!“ (J 1, 43)
3. Ježíš, který řekl Šimonovi a Ondřejovi: „Pojďte za mnou, a udělám z vás rybáře lidí!“ a ihned nechali sítě a následovali ho. (srov. Mk 1, 17-18)
4. Ježíš, který, uviděl Zebedeova syna Jakuba a jeho bratra Jana, jak na lodi spravují sítě; a hned je povolal. Zanechali svého otce Zebedea s pomocníky na lodi a odešli za ním. (srov. Mk 1, 19-20)
5. Ježíš, který když uviděl v celnici sedět člověka, který se jmenoval Matouš, mu řekl: „Pojď za mnou!“ a on vstal a šel za ním. (srov. Mt 9, 9)
6. Ježíš, který vystoupil na horu, zavolal k sobě ty, které sám chtěl, a oni přišli k němu. (Mk 3, 13)
7. Ježíš, který vyslal apoštoly, a oni se vydali na cesty a procházeli vesnice, všude hlásali radostnou zvěst a uzdravovali. (srov. Lk 9, 6)
8. Ježíš, který řekl: „Nepřišel jsem totiž povolat spravedlivé, ale hříšníky.“ (Mt 9, 13)
9. Ježíš, který s láskou pohlédl na bohatého mladíka a řekl: „Jedno ti schází: Jdi, prodej všechno, co máš, a rozdej chudým, a budeš mít poklad v nebi. Pak přijď a následuj mě!“ (Mk 10, 21)
10. Ježíš, který řekl: „Nikdo ke mně nemůže přijít, není-li mu to dáno od Otce.“ (J 6, 65)

## II. desátek
1. Ježíš, který se modlil k Otci: „Jako jsi mne poslal do světa, tak i já jsem je poslal do světa.“ (J 17, 18)
2. Ježíš, který řekl: „Já jsem vás poslal sklízet, na čem jste ne pracovali. Jiní pracovali, a vy sklízíte plody jejich práce.“ (J 4, 38)
3. Ježíš, který řekl: „Když vytrváte v mém slovu, budete opravdu mými učedníky. Poznáte pravdu, a pravda vás osvobodí.“ (J 8, 31-32)
4. Ježíš, který řekl: „Musíme konat skutky toho, který mě poslal, dokud je den. Přichází noc, kdy nikdo nebude moci pracovat.“ (J 9, 4)
5. Ježíš, který řekl: „Amen, amen, pravím vám: služebník není víc než jeho pán, posel není víc než ten, kdo ho poslal.“ (J 13, 16)
6. Ježíš, který řekl: „Už vás nenazývám služebníky, protože služebník neví, co dělá jeho pán.“ (J 15, 15a)
7. Ježíš, který řekl: „Nazval jsem vás přáteli, protože vám jsem oznámil všechno, co jsem slyšel od svého Otce.“ (J 15, 15b)
8. Ježíš, který řekl: „Ne vy jste si vyvolili mne, ale já jsem vyvolil vás a určil jsem vás k tomu, abyste šli a přinášeli užitek a váš užitek aby byl trvalý.“ (J 15, 16a)
9. Ježíš, který řekl apoštolům: „Pokoj vám! Jako otec poslal mne, tak i já posílám vás.“ (J 20, 21)
10. Ježíš, který na ně dechl a řekl: „Přijměte Ducha svatého! Komu hříchy odpustíte, tomu jsou odpuštěny, komu je neodpustíte, tomu odpuštěny nejsou.“ (J 20, 22b-23)

## III. desátek
1. Ježíš, který jich ustanovil dvanáct, aby byli s ním, protože je chtěl posílat kázat, a to s mocí vyhánět zlé duchy. (srov. Mk 3, 14-15)
2. Ježíš, který řekl: „Hle, já vás posílám jako ovce mezi vlky! Buďte tedy opatrní jako hadi a bezelstní jako holubice.“ (Mt 10, 16)
3. Ježíš, který řekl: „Co vám říkám ve tmě, povězte na světle, a co se vám šeptá do ucha, hlásejte ze střech!“ (Mt 10, 27)
4. Ježíš, který řekl: „Když někde vejdete do domu, napřed řekně-te: Pokoj tomuto domu! Bude-li tam člověk hodný pokoje, spočine na něm váš pokoj, jinak se vrátí k vám.“ (Lk 10, 5-6)
5. Ježíš, který řekl: „Když při jdete do některého města a přijmou vás tam, jezte, co vám předloží, uzdravujte tamější nemocné a říkejte jim: Přiblížilo se k vám Boží království“ (Lk 10, 8-9)
6. Ježíš, který řekl: „Dal jsem vám moc šlapat na hady, štíry a přemáhat všechnu nepřítelovu sílu a vůbec nic vám nebude moci uškodit.“ (Lk 10, 19)
7. Ježíš, který řekl Šimonovi: „Pas mé beránky! (…) Pas moje ovce!“ (J 21, 15c.16)
8. Ježíš, který k nim přistoupil a promluvil: „Je mi dána veškerá moc na nebi i na zemi. Jděte tedy, získejte za učedníky všechny národy, křtěte je ve jménu Otce i Syna i Ducha svatého.“ (Mt 28, 18-19)
9. Ježíš, který řekl: „Učte je zachovávat všechno, co jsem vám přikázal. Hle, já jsem s vámi po všechny dny až do konce světa!“ (Mt 28, 20)
10. Ježíš, který řekl: „Žádný, kdo položil ruku na pluh a ohlíží se za sebe, není způsobilý pro Boží království!“ (Lk 9, 62)

## IV. desátek
1. Ježíš, který řekl: „Vám je dáno tajemství Božího království, ale ostatním, kdo jsou mimo, se všechno předkládá v podobenstvích.“ (Mk 4, 11)
2. Ježíš, který řekl: „Kdo věří ve mne, i ten bude konat skutky, které já konám, ba ještě větší bude konat, protože já odcházím k Otci.“ (J 14, 12)
3. Ježíš, který řekl: „Za cokoli budete prosit ve jménu mém, to všechno udělám, aby Otec byl oslaven v Synovi.“ (J 14, 13)
4. Ježíš, který řekl: „Svět Ducha Pravdy nemůže přijmout, protože ho nevidí a nezná. Vy ho znáte, neboť přebývá u vás a bude ve vás.“ (J 14, 17)
5. Ježíš, který řekl: „Vy jste už čistí tím slovem, které jsem k vám mluvil. Zůstaňte ve mně, a já zůstanu ve vás.“ (J 15, 3-4a)
6. Ježíš, který řekl: „Já jsem vinný kmen, vy jste ratolesti. Kdo zůstává ve mně a já v něm, ten nese mnoho ovoce, neboť beze mne nemůžete dělat nic.“ (J 15, 5)
7. Ježíš, který řekl: „Tím bude oslaven můj Otec, že ponesete mnoho ovoce a osvědčíte se jako moji učedníci.“ (J 15, 8)
8. Ježíš, který řekl: „Zachováte-li moje přikázání, zůstanete v mé lásce, jako jsem já zachovával přikázání svého Otce a zůstá-vám v jeho lásce.“ (J 15, 10)
9. Ježíš, který řekl: „Když pronásledovali mne, budou pronásledovat i vás. Když zachovali moje slovo, budou zachovávat i vaše.“ (J 15, 20)
10. Ježíš, který řekl: „Vy také vydávejte svědectví, protože jste se mnou od začátku.“ (J 15, 27)

V. desátek
1. Ježíš, který řekl: „Velebím tě, Otče, Pane nebe a země, že když jsi tyto věci skryl před moudrými a chytrými, odhalil jsi je maličkým; ano, Otče, tak se ti zalíbilo.“ (Mt 11, 25-26)
2. Ježíš, který řekl: „Já budu prosit Otce, a dá vám jiného Přímluvce, aby s vámi zůstal navždy: Ducha pravdy.“ (J 14, 16-17a)
3. Ježíš, který řekl: „Já prosím za ně. Neprosím za svět, ale za ty, které jsi mi dal, vždyť jsou tvoji; a všechno mé je tvé a všechno tvé je mé. V nich jsem oslaven.“ (J 17, 9-10)
4. Ježíš, který řekl: „Už nejsem na světě, ale oni jsou na světě; a já jdu k tobě. Otče svatý, zachovej je ve svém jménu, které jsi mi dal, aby byli jedno jako my.“ (J 17, 11)
5. Ježíš, který řekl: „Neprosím, abys je ze světa vzal, ale abys je zachránil od Zlého.“ (J 17, 15)
6. Ježíš, který řekl: „Posvěť je v pravdě; tvé slovo je pravda.“ (J 17, 17)
7. Ježíš, který řekl: „Pro ně se zasvěcuji, aby i oni byli posvěceni v pravdě.“ (J 17, 19)
8. Ježíš, který řekl: „A slávu, kterou jsi dal mně, dal jsem já jim, aby byli jedno, jako my jsme jedno: já v nich a ty ve mně.“ (J 17, 22-23a)
9. Ježíš, který řekl: „Otče, chci, aby tam, kde jsem já, byli se mnou i ti, které jsi mi dal, aby viděli mou slávu, kterou jsi mi dal, protože jsi mě miloval už před založením světa.“ (J 17, 24)
10. Ježíš, který řekl Šimonovi: „Šimone, Šimone, satan si vyžádal, aby vás směl protříbit jako pšenici, ale já jsem za tebe prosil, aby tvoje víra nezanikla. A ty potom, až se obrátíš, utvrzuj své bratry.” (Lk 22, 31-32)
