---
title: Materiály
---

## Tajemství jednotlivých desátků

  - [Radostný](materialy/radostny.md)
- [Světla](materialy/svetla.md)
- [Bolestný](materialy/bolestny.md)
- [Slavný](materialy/slavny.md)
- [Kněžská povolání](materialy/knezska-povolani.md)

## Meditace

_Zdroj: [Salesiáni spolupracovníci](http://www.ascczech.cz/) - [Formační plány na roky 2010 a 2011](http://www.ascczech.cz/rs/download.php?sekce=5)._

- [Radostný a slavný](/materialy/meditace/ASC_radostny_slavny.pdf)
- [Světla a bolestný](/materialy/meditace/ASC_svetla_bolestny.pdf)
