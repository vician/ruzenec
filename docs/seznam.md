## Stretko, Brno

### [1. skupina](/skupina/?m=Hanka,Markéta,Lukáš,Frencis,Vendy&s=2010-07-26)

<div id="stretko1"></div>

### [2. skupina](/skupina/?m=Martin,Mária,Pavel,Dominik,Marie&s=2010-06-21)

<div id="stretko2"></div>

## Bývalí středoškoláci, České Budějovice

### [1. skupina](/skupina/?m=Adéla,Markéta F.,Magda,Markéta K., Šárka&s=2010-04-12)

<div id="cb1"></div>

<script>
window.onload = function () {
	draw_table("Hanka,Markéta,Lukáš,Frencis,Vendy", "2010-07-26", "stretko1");
	draw_table("Martin,Mária,Pavel,Dominik,Marie", "2010-06-21", "stretko2");
	draw_table("Adéla,Markéta F.,Magda,Markéta K., Šárka", "2010-04-12", "cb1");
}
</script>
